'use strict';
var settingsPromise,
    koiId,
    attachmentSettings;

function _loadSettings () {
    var url = attachmentsApi.settings;

    settingsPromise = $.get(url)
        .done(function (data) {
            attachmentSettings = data;
        })
        .fail(function () {
            alert('Error: loading settings');
        });
}

// ---- attachments tab ----
function _getUniqueAttachments (rawAttachmentData) {
    var result = [];

    if ($.isArray(rawAttachmentData)) {
        for (var i = 0, obj = {}; i < rawAttachmentData.length; i++) {
            if ($.isPlainObject(rawAttachmentData[i]) && ('file_id' in rawAttachmentData[i])) {
                var fileId = rawAttachmentData[i]['file_id'];

                obj[fileId] = rawAttachmentData[i];
            }
        }
        for (fileId in obj) {
            if (obj.hasOwnProperty(fileId)) {
                result.push(obj[fileId]);
            }
        }
    }
    return result;
}

function _buildAttachmentsPanelContents (attachmentData) {
    var dialog = CKEDITOR.dialog.getCurrent(),
        attachmentPanelInnerMarkup = '',
        uniqueAttachments = _getUniqueAttachments(attachmentData);

    for (var i = 0; i < uniqueAttachments.length; i++) {
        var thumbUrl = uniqueAttachments[i]['thumbnail_url'],
            fileId = uniqueAttachments[i]['file_id'];

        if (fileId && thumbUrl) {
            attachmentPanelInnerMarkup +=
                '<div class="attachment-thumb">' +
                    '<img src="' + thumbUrl + '" alt="" data-file-id="' + fileId + '" />' +
                '</div>';
        }
    }

    $('#attachment-panel').append(attachmentPanelInnerMarkup);

    $('#attachment-panel').on('click', '.attachment-thumb', function () {
        var $thumb = $(this),
            $image = $thumb.find('img'),
            url = $image.attr('src'),
            fileId = $image.data('file-id');

        $thumb.parents('#attachment-panel').find('.attachment-thumb').removeClass('selected');
        $thumb.addClass('selected');

        dialog.setValueOf('tab-attachments', 'url', url);
        koiId = fileId;
    });
}

function loadAttachments () {
    var url = attachmentsApi.files;

    $.get(url)
        .done(_buildAttachmentsPanelContents)
        .error(function () {
            alert('Error: loading attachments');
        });
}
// ---- end of attachments tab ----

// ---- local file tab ----
function _buildHelpText (editor) {
    var maxFileSize = attachmentSettings['max_file_size'],
        fileTypeList = attachmentSettings['extensions'];

    return '<p class="help-text">' + editor.lang.mcImage.fileUploadHelpText(maxFileSize, fileTypeList) + '</p>';
}

function addHelpText (editor) {
    settingsPromise.done(function () {
        $('#upload-panel').prepend(_buildHelpText(editor));
    });
}

function initFileUploader (editor) {
    var dialog = CKEDITOR.dialog.getCurrent();

    settingsPromise.done(function () {
        $('#fileupload').fileupload({
            url: attachmentsApi.files,
            dataType: 'json',
            formData: {'post_hash': postHash},
            dropZone: null,
            autoUpload: false,
            replaceFileInput: false,

            add: function(e, data) {
                var fileType = data.originalFiles[0]['type'],
                    fileSize = data.originalFiles[0]['size'],
                    uploadErrors = [];

                if (fileType && fileSize) {
                    var patternStr = '^image/' + attachmentSettings.extensions.join('|') + '$',
                        acceptFileTypes = new RegExp(patternStr, 'i'),
                        maxFileSize = attachmentSettings['max_file_size'] * 1000000;

                    if (fileType.length && !acceptFileTypes.test(fileType)) {
                        uploadErrors.push(editor.lang.mcImage.incorrectFileTypeMsg);
                    }
                    if (fileSize && fileSize > maxFileSize) {
                        uploadErrors.push(editor.lang.mcImage.tooLargeFileMsg);
                    }
                }
                if (uploadErrors.length > 0) {
                    alert(uploadErrors.join('\n'));
                }
                else {
                    var uploadButton = dialog.getContentElement('tab-local', 'upload-local');

                    uploadButton.once('click', function () {
                        uploadButton.disable();
                        data.submit();
                    });
                    uploadButton.enable();
                }
            },
            done: function (event, data) {
                var thumbUrl = data.result['thumbnail_url'],
                    fileId = data.result['file_id'];

                if (fileId && thumbUrl) {
                    dialog.setValueOf('tab-local', 'url', thumbUrl);
                    koiId = fileId;

                    $('#fileupload').prop('disabled', true);
                }
                else {
                    alert('Error: uploading file');
                    $('#fileupload').replaceWith($(this).clone(true));
                }
            },
            fail: function () {
                alert('Error: uploading file');
                $('#fileupload').replaceWith($(this).clone(true));
            }
        });
    });
}
// ---- end of local file tab ----

// ---- external tab ----
function uploadExternalImage (button) {
    var dialog = CKEDITOR.dialog.getCurrent(),
        externalUrlField = dialog.getContentElement('tab-external', 'external-url'),
        uploadButton = dialog.getContentElement('tab-external', 'upload-external');

    var url = attachmentsApi.files,
        externalImageUrl = dialog.getValueOf('tab-external', 'external-url');

    if (externalImageUrl) {
        externalUrlField.disable();
        uploadButton.disable();

        $.post(url, {
            'post_hash': postHash || null,
            'file_url': externalImageUrl
        })
            .done(function (data) {
                var thumbUrl = data['thumbnail_url'],
                    fileId = data['file_id'];

                if (fileId && thumbUrl) {
                    dialog.setValueOf('tab-external', 'url', thumbUrl);
                    koiId = fileId;
                }
                else {
                    alert('Error: uploading external image');
                    externalUrlField.enable();
                    uploadButton.enable();
                }
            })
            .error(function () {
                alert('Error: uploading external image');
                externalUrlField.enable();
                uploadButton.enable();
            });
    }
}
// ---- end of external tab ----

CKEDITOR.dialog.add('mcImageDialog', function (editor) {
    return {
        title: editor.lang.mcImage.dialogTitle,
        minWidth: 400,
        minHeight: 200,

        contents: [
            {
                id: 'tab-attachments',
                label: editor.lang.mcImage.attachmentsTabTitle,
                elements: [
                    {
                        type: 'html',
                        html: '<div id="attachment-panel"></div>',
                        onLoad: loadAttachments
                    },
                    {
                        type: 'text',
                        id: 'url',
                        label: editor.lang.mcImage.externalUrlLabel,
                        className: 'hidden',
                        onShow: function () {
                            this.disable();
                        }
                    },
                    {
                        type: 'text',
                        id: 'alt',
                        label: editor.lang.mcImage.externalAltLabel
                    }
                ]
            },
            {
                id: 'tab-local',
                label: editor.lang.mcImage.localTabTitle,
                elements: [
                    {
                        type: 'html',
                        html: '<div id="upload-panel"><input id="fileupload" type="file" name="file"/></div>',
                        onLoad: function () {
                            addHelpText(editor);
                        },
                        onShow: function () {
                            initFileUploader(editor);
                        }
                    },
                    {
                        type: 'button',
                        id: 'upload-local',
                        disabled: true,
                        label: editor.lang.mcImage.localButtonLabel
                    },
                    {
                        type: 'text',
                        id: 'url',
                        label: editor.lang.mcImage.localUrlLabel,
                        onShow: function () {
                            this.disable();
                        }
                    },
                    {
                        type: 'text',
                        id: 'alt',
                        label: editor.lang.mcImage.localAltLabel
                    }
                ]
            },
            {
                id: 'tab-external',
                label: editor.lang.mcImage.urlTabTitle,
                elements: [
                    {
                        type: 'text',
                        id: 'external-url',
                        label: editor.lang.mcImage.externalUrlLabel
                    },
                    {
                        type: 'text',
                        id: 'url',
                        label: editor.lang.mcImage.externalUrlLabel,
                        className: 'hidden'
                    },
                    {
                        type: 'text',
                        id: 'alt',
                        label: editor.lang.mcImage.externalAltLabel
                    },
                    {
                        type: 'button',
                        id: 'upload-external',
                        label: editor.lang.mcImage.externalButtonLabel,
                        onClick: function () {
                            var uploadButton = this;

                            uploadExternalImage(uploadButton);
                        }
                    }
                ]
            }
        ],

        onLoad: function () {
            var dialog = this;

            _loadSettings();

            this.on('selectPage', function (event) {
                dialog.setValueOf(event.data.currentPage, 'url', '');
                dialog.setValueOf(event.data.currentPage, 'alt', '');

                $('#attachment-panel .attachment-thumb').removeClass('selected');
            });
        },
        onOk: function () {
            var dialog = this,

                attachmentUrl = dialog.getValueOf('tab-attachments', 'url'),
                attachmentAlt = dialog.getValueOf('tab-attachments', 'alt'),
                localUrl = dialog.getValueOf('tab-local', 'url'),
                localAlt = dialog.getValueOf('tab-local', 'alt'),
                externalUrl = dialog.getValueOf('tab-external', 'url'),
                externalAlt = dialog.getValueOf('tab-external', 'alt'),

                url = attachmentUrl || localUrl || externalUrl,
                alt = attachmentAlt || localAlt || externalAlt;

            if (url) {
                var imgMarkup = '<img src="' + url + '" alt="' + alt + '" data-koi-id="' + koiId + '"/>',
                    imgElement = CKEDITOR.dom.element.createFromHtml(imgMarkup);

                editor.insertElement(imgElement);
            }

            $('#attachment-panel .attachment-thumb').removeClass('selected');
        },
        onCancel: function () {
            $('#attachment-panel .attachment-thumb').removeClass('selected');
        }
    };
});
