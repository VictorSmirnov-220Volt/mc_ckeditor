'use strict';
CKEDITOR.plugins.setLang('mcImage', 'ru', {
    buttonLabel: '�������� �����������',
    dialogTitle: '�������� �����������',

    attachmentsTabTitle: '����� �����������',

    localTabTitle: '� ����������',
    localFileLabel: '�������',
    localUrlLabel: '������',
    localAltLabel: '�������������� �����',
    localButtonLabel: '���������',
    fileUploadHelpText: function (maxFileSize, fileTypeList) {
        return '����������� � ������� ' + fileTypeList.join(', ').toUpperCase() + ' �������� �� ' + maxFileSize + ' MB';
    },
    incorrectFileTypeMsg: '������������ ������ �����',
    tooLargeFileMsg: '�������� ������������ ������ �����',

    urlTabTitle: '�� ������ �� ���������',
    externalUrlLabel: '������',
    externalAltLabel: '�������������� �����',
    externalButtonLabel: '���������'
});
