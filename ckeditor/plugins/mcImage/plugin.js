'use strict';
CKEDITOR.plugins.add('mcImage', {
    requires: 'dialog',
    lang: 'ru',
    icons: 'image',

    init: function(editor) {
        editor.addCommand('mcImage', new CKEDITOR.dialogCommand('mcImageDialog'));

        editor.ui.addButton('Image', {
            label: editor.lang.mcImage.buttonLabel,
            command: 'mcImage',
            toolbar: 'insert,0'
        });

        CKEDITOR.dialog.add('mcImageDialog', this.path + 'dialogs/mcImage.js' );
    }
});
