'use strict';
var postHash = md5(new Date().getTime().toString());

$('#post-hash').val(postHash);

var attachmentsApi = {
        settings: '/local-api/settings',
        files: '/local-api/koi/files'
    };

var replyEditor = CKEDITOR.replace('reply-editor', {
        skin: 'mastercity',
        removePlugins: 'image',
        extraPlugins: 'mcImage',
        toolbar: [
            ['Bold', 'Italic', 'Underline', 'Strike'],
            ['Image', 'Youtube', 'Link', 'Smiley', 'Blockquote'],
            ['RemoveFormat']
        ],
        defaultLanguage: 'ru',
        on: {
            change: function (event) {
                var editorData = event.editor.getData();

                if (editorData.length) {
                    $('#reply-submit').prop('disabled', false);
                }
                else {
                    $('#reply-submit').prop('disabled', true);
                }
            }
        }
    });

$('#reply-form-switcher').click(function () {
    var $form = $(this).closest('.reply-form');

    $form.toggleClass('tiny').find('.reply-extended').prop('disabled', $form.hasClass('tiny'));

    replyEditor.resize('100%', ($form.hasClass('tiny') ? 200 : 300), true);
});

$('.star-radio input[type="radio"]').change(function () {
    $('.star-radio .star').removeClass('star-checked');
    $('.star-radio :checked').closest('.star-radio').prevAll('.star-radio').find('.star').addClass('star-checked');
});

$('input[name="subscribe"]').change(function () {
    $('select[name="emailupdate"]').prop('disabled', !$(this).is(':checked'));
});

$('.reply-form').on('reset', function () {
    replyEditor.setData('');
});

$('.quickreply').on('click', function (event) {
    event.preventDefault();

    var username = $(this).closest('.postcontainer').find('.username').text(),
        markup = '<strong>@' + username + '</strong>',
        el = CKEDITOR.dom.element.createFromHtml(markup);

    replyEditor.insertElement(el);
});
