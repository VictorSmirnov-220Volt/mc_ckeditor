'use strict';
var http = require('http'),
    url = require('url'),
    fs = require('fs');

fs.readFile('./index.html', function (err, html) {
    if (err) {
        throw err;
    }
    http.createServer(function (request, response) {
        response.setHeader('Access-Control-Allow-Origin', '*');

        var pathname = url.parse(request.url).pathname;

        console.log(request.method, pathname);

        switch (pathname) {
            case '/local-api/settings':
            case '/local-api/settings/':
                switch (request.method) {
                    case 'GET':
                        response.setHeader('Content-Type', 'application/json');
                        response.end(JSON.stringify({
                            "extensions": [
                                "gif", "jpg", "jpeg", "png", "bmp"
                            ],
                            "img": {
                                "min_height": 240,
                                "min_width": 320,
                                "thumbnail_height": 80,
                                "thumbnail_width": 100
                            },
                            "max_file_size": 10
                        }));
                        console.log('settings');
                        break;
                    default:
                        response.write('Method', request.method, 'not allowed');
                }
                break;
            case '/local-api/koi/files':
            case '/local-api/koi/files/':
                switch (request.method) {
                    case 'POST':
                        response.setHeader('Content-Type', 'application/json');
                        response.end(JSON.stringify({
                            "file_id": 6,
                            "url": "http://lorempixel.com/320/240/food/6/",
                            "thumbnail_url": "http://lorempixel.com/100/80/food/6/"
                        }));
                        console.log('file or url');
                        break;
                    case 'GET':
                        response.setHeader('Content-Type', 'application/json');
                        response.end(JSON.stringify([
                            {
                                "info_id": 1,
                                "file_id": 1,
                                "thread_id": 10,
                                "post_id": 100,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/1/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/1/"
                            },
                            {
                                "info_id": 2,
                                "file_id": 1,
                                "thread_id": 11,
                                "post_id": 101,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/1/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/1/"
                            },
                            {
                                "info_id": 1,
                                "file_id": 2,
                                "thread_id": 12,
                                "post_id": 102,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/2/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/2/"
                            },
                            {
                                "info_id": 1,
                                "file_id": 3,
                                "thread_id": 13,
                                "post_id": 103,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/3/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/3/"
                            },
                            {
                                "info_id": 1,
                                "file_id": 4,
                                "thread_id": 14,
                                "post_id": 104,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/4/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/4/"
                            },
                            {
                                "info_id": 1,
                                "file_id": 5,
                                "thread_id": 15,
                                "post_id": 105,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/5/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/5/"
                            },
                            {
                                "info_id": 2,
                                "file_id": 5,
                                "thread_id": 16,
                                "post_id": 106,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/5/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/5/"
                            },
                            {
                                "info_id": 3,
                                "file_id": 5,
                                "thread_id": 17,
                                "post_id": 107,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/5/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/5/"
                            },
                            {
                                "info_id": 4,
                                "file_id": 5,
                                "thread_id": 18,
                                "post_id": 108,
                                "user_id": 21,
                                "url": "http://lorempixel.com/320/240/food/5/",
                                "thumbnail_url": "http://lorempixel.com/100/80/food/5/"
                            }
                        ]));
                        console.log('attachments');
                        break;
                    case 'OPTIONS':
                        break;
                    default:
                        response.write('Method', request.method, 'not allowed');
                }
                break;
            case '/newreply.php':
                switch (request.method) {
                    case 'POST':
                        response.end('Posted!');
                        console.log('new reply');
                        break;
                    default:
                        response.write('Method', request.method, 'not allowed');
                }
                break;
            case '/':
                response.setHeader('Content-Type', 'text/html; charset=windows-1251');
                response.end(html);
                break;
            default:
                response.end('My super mock!');
                break;
        }
        //response.end();
    }).listen(8888);

    console.log('Mock server started on http://localhost:8888');
    console.log('============================================');
});
