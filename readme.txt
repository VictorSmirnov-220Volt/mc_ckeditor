Как заставить всё работать:
1. CKEditor скопировать вместе с папкой (в нём модифицированные плагины и собственный плагин)
2. Убедиться, что до инициализации CKEditor (до подключения init-reply.js) подключены:
    - jQuery,
    - сам CKEditor (ckeditor/ckeditor.js)
    - md5.min.js
3. Подключить скрипты, необходимые для загрузки файлов на сервер:
    - jquery.ui.widget.js
    - jquery.iframe-transport.js
    - jquery.fileupload.js
4. Подключить файл со стилями (custom_ckeditor.css)
5. В файле init-reply.js прописать корректные пути к методам API (переменная attachmentsApi)

Внимание! Папку env копировать не нужно. В ней лишь файлы, используемые при разработке.
